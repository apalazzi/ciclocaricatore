EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ciclocaricatore-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L AC #PWR1
U 1 1 59667658
P 2950 3250
F 0 "#PWR1" H 2950 3150 50  0001 C CNN
F 1 "AC" H 2950 3500 50  0000 C CNN
F 2 "" H 2950 3250 50  0001 C CNN
F 3 "" H 2950 3250 50  0001 C CNN
	1    2950 3250
	1    0    0    -1  
$EndComp
$Comp
L D_Zener D2
U 1 1 59667736
P 3450 3900
F 0 "D2" H 3450 4000 50  0000 C CNN
F 1 "D_Zener" H 3450 3800 50  0000 C CNN
F 2 "" H 3450 3900 50  0001 C CNN
F 3 "" H 3450 3900 50  0001 C CNN
	1    3450 3900
	0    -1   -1   0   
$EndComp
$Comp
L D_Zener D1
U 1 1 59667859
P 3450 3500
F 0 "D1" H 3450 3600 50  0000 C CNN
F 1 "D_Zener" H 3450 3400 50  0000 C CNN
F 2 "" H 3450 3500 50  0001 C CNN
F 3 "" H 3450 3500 50  0001 C CNN
	1    3450 3500
	0    1    1    0   
$EndComp
$Comp
L D_Bridge_+-AA D3
U 1 1 596676D7
P 4250 3700
F 0 "D3" H 4300 3975 50  0000 L CNN
F 1 "D_Bridge_+-AA" H 4300 3900 50  0000 L CNN
F 2 "" H 4250 3700 50  0001 C CNN
F 3 "" H 4250 3700 50  0001 C CNN
	1    4250 3700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR2
U 1 1 59667A0D
P 2950 4200
F 0 "#PWR2" H 2950 3950 50  0001 C CNN
F 1 "GND" H 2950 4050 50  0000 C CNN
F 2 "" H 2950 4200 50  0001 C CNN
F 3 "" H 2950 4200 50  0001 C CNN
	1    2950 4200
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 59667A40
P 5200 4550
F 0 "C1" H 5225 4650 50  0000 L CNN
F 1 "C" H 5225 4450 50  0000 L CNN
F 2 "" H 5238 4400 50  0001 C CNN
F 3 "" H 5200 4550 50  0001 C CNN
	1    5200 4550
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 59667D20
P 7750 4450
F 0 "R1" V 7830 4450 50  0000 C CNN
F 1 "R" V 7750 4450 50  0000 C CNN
F 2 "" V 7680 4450 50  0001 C CNN
F 3 "" H 7750 4450 50  0001 C CNN
	1    7750 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 3750 3450 3650
Connection ~ 3450 4050
Wire Wire Line
	2950 3350 4250 3350
Wire Wire Line
	4250 3350 4250 3400
Wire Wire Line
	2950 4050 4250 4050
Wire Wire Line
	4250 4050 4250 4000
Wire Wire Line
	2950 3250 2950 3350
Connection ~ 3450 3350
Wire Wire Line
	2950 4050 2950 4200
Wire Wire Line
	4550 3700 6550 3700
Wire Wire Line
	5200 3700 5200 4400
Wire Wire Line
	3950 3700 3950 4950
Wire Wire Line
	5200 4950 5200 4700
Wire Wire Line
	6550 3700 6550 3900
Connection ~ 5200 3700
Connection ~ 5200 4950
Wire Wire Line
	7750 4950 7750 4600
Wire Wire Line
	7750 3900 7750 4300
Wire Wire Line
	7350 3900 7750 3900
Wire Wire Line
	3950 4950 7750 4950
Connection ~ 6950 4950
Wire Wire Line
	6950 4950 6950 4200
$Comp
L 7805 U1
U 1 1 59667C05
P 6950 3950
F 0 "U1" H 7100 3754 50  0000 C CNN
F 1 "7805" H 6950 4150 50  0000 C CNN
F 2 "" H 6950 3950 50  0001 C CNN
F 3 "" H 6950 3950 50  0001 C CNN
	1    6950 3950
	1    0    0    -1  
$EndComp
$EndSCHEMATC
